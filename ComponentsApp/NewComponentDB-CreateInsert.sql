CREATE TABLE [dbo].[Components] (
    [SKU]              INT             IDENTITY (1, 1) NOT NULL,
    [Name]             VARCHAR (50)    NOT NULL,
    [Description]      VARCHAR (MAX)   NULL,
    [Price]            MONEY           NOT NULL,
    [Category]         VARCHAR (MAX)   NOT NULL,
    [Subcategory]      VARCHAR (MAX)   NOT NULL,
    [DateFirstInStock] DATETIME        NULL,
    [Image]            VARBINARY (MAX) NULL,
    [Available]        BIT             NULL,
    PRIMARY KEY CLUSTERED ([SKU] ASC)
);

CREATE TABLE [dbo].[Users] (
    [UserId]    INT           IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (MAX) NOT NULL,
    [LastName]  VARCHAR (MAX) NOT NULL,
    [UserName]  VARCHAR (50)  NOT NULL,
    [Password]  VARCHAR (MAX) NOT NULL,
    [Admin]     BIT           NOT NULL,
    CONSTRAINT [PK_Table] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

SET IDENTITY_INSERT [dbo].[Users] ON
INSERT INTO [dbo].[Users] ([UserId], [FirstName], [LastName], [UserName], [Password], [Admin]) VALUES (3, N'Kevin', N'Dunne', N'KDunne', N'PU8r8H3BvjiyDNbkaUmhBx+dDj0=', 1)
INSERT INTO [dbo].[Users] ([UserId], [FirstName], [LastName], [UserName], [Password], [Admin]) VALUES (4, N'Dean', N'Dempsey', N'DDempsey', N'JzoMe9PGebqab12ZB4426F0CuVI=', 0)
INSERT INTO [dbo].[Users] ([UserId], [FirstName], [LastName], [UserName], [Password], [Admin]) VALUES (5, N'Kate', N'White', N'Kwhite', N'd7zp+xj5d+pXa7zRQ7K1IQc/DNY=', 0)
SET IDENTITY_INSERT [dbo].[Users] OFF
