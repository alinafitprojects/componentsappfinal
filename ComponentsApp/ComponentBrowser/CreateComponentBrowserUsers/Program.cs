﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CreateComponentBrowserUsers
{
    class Program
    {
        static void Main(string[] args)
        {

            string connection = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=ComponentDB;Integrated Security=True;Connect Timeout=4;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

            // Connect to database
            SqlConnection conn = new SqlConnection(connection);

            // Try and open a connection to the DB
            conn.Open();

            // ask user for username
            Console.Write("Give me a username please : ");
            string username = Console.ReadLine().Trim().ToLower();
            // ask user for password
            Console.Write("Give me a password please : ");
            string password = Console.ReadLine().Trim();

            //ask for First name and last name
            Console.Write("Enter the first name: ");
            string firstname = Console.ReadLine().Trim();

            Console.Write("Enter the last name: ");
            string lastname = Console.ReadLine().Trim();

            //is the user an admin?
            Console.Write("is the user an admin (enetr y/n): ");
            bool admin = (Console.ReadLine().Trim().ToLower().StartsWith("y")) ? true : false;

            // hash the password
            var sha1 = new SHA1CryptoServiceProvider();
            byte[] bytes = Encoding.UTF8.GetBytes(password);
            string result = Convert.ToBase64String(sha1.ComputeHash(bytes)); // store it

            // Show the password
            Console.WriteLine("The hashed password is: " + result);


            // write usernames and hashed passwords to the db
            string command = $"INSERT INTO users VALUES ('{firstname}', '{lastname}', '{username}','{result}','{admin}')";
            // Create a command that we will execute on our DB connection 'conn'
            SqlCommand insertUser = new SqlCommand(command, conn);

            try
            {
                // Execute the command
                insertUser.ExecuteNonQuery();
                Console.WriteLine("User {0} Created", username);
            }
            catch (SqlException se)
            {
                Console.WriteLine(se.Message);
                   
            }

            finally
            {
                // close the DB connection
                conn.Close();
            }

            
          

            Console.ReadLine();
        } 

        }
    }

