﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComponentBrowser
{
    public partial class FrmSearch : Form

    {
        private FrmComponentBrowser returnWindow;
        private DataView dataView;
        private DataSet ds;
        private BindingSource bs;

        public FrmSearch(DataSet ds, FrmComponentBrowser browserWindow)
        {
            InitializeComponent();
            this.returnWindow = browserWindow;
            this.ds = ds;

            bs = new BindingSource();
            bs.DataSource = ds.Tables[0];

            dataView = new DataView(ds.Tables[0]);
            dtGV.DataSource = dataView;
            dtGV.RowTemplate.Height = 100;
        }
               
		// Add records to the combobox filter fields
        private void Search_Load(object sender, EventArgs e)
        {
            cbSchF1.Items.Add("Id");
            cbSchF1.Items.Add("Name");
            cbSchF1.Items.Add("Description");
            cbSchF1.Items.Add("Price");
            cbSchF1.Items.Add("Category");
            cbSchF1.Items.Add("Subcategory");
            cbSchF1.Items.Add("DateFirstInStock");
            cbSchF1.Items.Add("Price");
            cbSchF1.Items.Add("Available");

            cbSchF2.Items.Add("Id");
            cbSchF2.Items.Add("Name");
            cbSchF2.Items.Add("Description");
            cbSchF2.Items.Add("Price");
            cbSchF2.Items.Add("Category");
            cbSchF2.Items.Add("Subcategory");
            cbSchF2.Items.Add("DateFirstInStock");
            cbSchF2.Items.Add("Price");
            cbSchF2.Items.Add("Available");

            cbSchF3.Items.Add("Id");
            cbSchF3.Items.Add("Name");
            cbSchF3.Items.Add("Description");
            cbSchF3.Items.Add("Price");
            cbSchF3.Items.Add("Category");
            cbSchF3.Items.Add("Subcategory");
            cbSchF3.Items.Add("DateFirstInStock");
            cbSchF3.Items.Add("Price");
            cbSchF3.Items.Add("Available");

            cbOp1.Items.Add("=");
            cbOp1.Items.Add("Like");
            cbOp1.Items.Add("<");
            cbOp1.Items.Add(">");
            cbOp1.Items.Add(">=");
            cbOp1.Items.Add("=<");

            cbOp2.Items.Add("=");
            cbOp2.Items.Add("Like");
            cbOp2.Items.Add("<");
            cbOp2.Items.Add(">");
            cbOp2.Items.Add(">=");
            cbOp2.Items.Add("=<");

            cbOp3.Items.Add("=");
            cbOp3.Items.Add("Like");
            cbOp3.Items.Add("<");
            cbOp3.Items.Add(">");
            cbOp3.Items.Add(">=");
            cbOp3.Items.Add("=<");
        
        }

		// Close window
        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
            returnWindow.Show();
        }

		// Filter records
        private void btnRunSearch_Click(object sender, EventArgs e)
        {
            string filter1 = null;
            string filter2 = null;
            string filter3 = null;

            List<string> filters = new List<string>();
            string filterFormat = "[{0}] {1}'{2}'";

            if (cbSchF1.SelectedItem != null && cbOp1.SelectedItem != null && !string.IsNullOrEmpty(txBxFilter1.Text))
            {
                filter1 = String.Format(filterFormat, 
                    cbSchF1.SelectedItem.ToString(), cbOp1.SelectedItem.ToString(), txBxFilter1.Text);

                filters.Add(filter1);
            }

            if (cbSchF2.SelectedItem != null && cbOp2.SelectedItem != null && !string.IsNullOrEmpty(txBxFilter2.Text))
            {
                filter2 = String.Format(filterFormat,
                    cbSchF2.SelectedItem.ToString(), cbOp2.SelectedItem.ToString(), txBxFilter2.Text);

                filters.Add(filter2);
            }

            if (cbSchF3.SelectedItem != null && cbOp3.SelectedItem != null && !string.IsNullOrEmpty(txBxFilter3.Text))
            {
                filter3 = String.Format(filterFormat,
                    cbSchF3.SelectedItem.ToString(), cbOp3.SelectedItem.ToString(), txBxFilter3.Text);

                filters.Add(filter3);
            }

            if (filters.Count == 0)
            {
                MessageBox.Show("Please check your search criteria and try again.");
            }
            else
            {
                string delimiter = " AND ";
                string fullFilter = filters.Aggregate((i, j) => i + delimiter + j);
                dataView.RowFilter = fullFilter;
            }
        }

		// Reset Filter
        private void btnReset_Click(object sender, EventArgs e)
        {
            cbSchF1.Text = null;
            cbOp1.Text = null;
            txBxFilter1.Text = null;
            cbSchF2.Text = null;
            cbOp2.Text = null;
            txBxFilter2.Text = null;
            cbSchF3.Text = null;
            cbOp3.Text = null;
            txBxFilter3.Text = null;

            dataView.RowFilter = null;
        }

		// Select record and go back to Component details window
        private void btnSelect_Click(object sender, EventArgs e)
        {
            if (dtGV.SelectedRows.Count > 0)
            {
                //get the SKU
                int componentID =Convert.ToInt32(dtGV.SelectedRows[0].Cells["SKU"].Value.ToString());

                returnWindow.MoveToComponentSKU(componentID);
                returnWindow.Show();
                Hide();
            }
            else
            {
                MessageBox.Show("Rows are not selected. Plese select a component");
            }
        }

        private void bindingSource1_CurrentChanged(object sender, EventArgs e)
        {
           
        }

        private void txBxFilter3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
