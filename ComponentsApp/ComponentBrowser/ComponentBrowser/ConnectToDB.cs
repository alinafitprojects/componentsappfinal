﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ComponentBrowser
{
    class ConnectToDB
    {
        private static string connectionInfo = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=ComponentDB;Integrated Security=True;Connect Timeout=4;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        private static SqlConnection dbConnection;

        public static SqlConnection GetDbConnection()
        {
            if (dbConnection == null)
            {
                try
                {
                    dbConnection = new SqlConnection(connectionInfo);
                    return dbConnection;
                }
                catch (SqlException ex)
                {
                    //rethrow the exception 
                    //e throw the exeption up the method call chain wich means that 
                    //the caller should to deal with the posibility of an exception 
                    throw ex;
                }                
            }
            else{
                return dbConnection;
            }
        }
    } // end class
}
