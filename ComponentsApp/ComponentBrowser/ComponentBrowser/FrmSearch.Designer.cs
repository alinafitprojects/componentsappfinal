﻿namespace ComponentBrowser
{
    partial class FrmSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cbSchF1 = new System.Windows.Forms.ComboBox();
            this.cbSchF2 = new System.Windows.Forms.ComboBox();
            this.cbSchF3 = new System.Windows.Forms.ComboBox();
            this.cbOp1 = new System.Windows.Forms.ComboBox();
            this.cbOp2 = new System.Windows.Forms.ComboBox();
            this.cbOp3 = new System.Windows.Forms.ComboBox();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnRunSearch = new System.Windows.Forms.Button();
            this.dtGV = new System.Windows.Forms.DataGridView();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.xToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txBxFilter1 = new System.Windows.Forms.TextBox();
            this.txBxFilter3 = new System.Windows.Forms.TextBox();
            this.txBxFilter2 = new System.Windows.Forms.TextBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dtGV)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // cbSchF1
            // 
            this.cbSchF1.FormattingEnabled = true;
            this.cbSchF1.Location = new System.Drawing.Point(39, 54);
            this.cbSchF1.Margin = new System.Windows.Forms.Padding(4);
            this.cbSchF1.Name = "cbSchF1";
            this.cbSchF1.Size = new System.Drawing.Size(160, 24);
            this.cbSchF1.TabIndex = 0;
            // 
            // cbSchF2
            // 
            this.cbSchF2.FormattingEnabled = true;
            this.cbSchF2.Location = new System.Drawing.Point(39, 103);
            this.cbSchF2.Margin = new System.Windows.Forms.Padding(4);
            this.cbSchF2.Name = "cbSchF2";
            this.cbSchF2.Size = new System.Drawing.Size(160, 24);
            this.cbSchF2.TabIndex = 3;
            // 
            // cbSchF3
            // 
            this.cbSchF3.FormattingEnabled = true;
            this.cbSchF3.Location = new System.Drawing.Point(39, 149);
            this.cbSchF3.Margin = new System.Windows.Forms.Padding(4);
            this.cbSchF3.Name = "cbSchF3";
            this.cbSchF3.Size = new System.Drawing.Size(160, 24);
            this.cbSchF3.TabIndex = 6;
            // 
            // cbOp1
            // 
            this.cbOp1.FormattingEnabled = true;
            this.cbOp1.Location = new System.Drawing.Point(221, 55);
            this.cbOp1.Margin = new System.Windows.Forms.Padding(4);
            this.cbOp1.Name = "cbOp1";
            this.cbOp1.Size = new System.Drawing.Size(55, 24);
            this.cbOp1.TabIndex = 1;
            // 
            // cbOp2
            // 
            this.cbOp2.FormattingEnabled = true;
            this.cbOp2.Location = new System.Drawing.Point(221, 101);
            this.cbOp2.Margin = new System.Windows.Forms.Padding(4);
            this.cbOp2.Name = "cbOp2";
            this.cbOp2.Size = new System.Drawing.Size(55, 24);
            this.cbOp2.TabIndex = 4;
            // 
            // cbOp3
            // 
            this.cbOp3.FormattingEnabled = true;
            this.cbOp3.Location = new System.Drawing.Point(221, 149);
            this.cbOp3.Margin = new System.Windows.Forms.Padding(4);
            this.cbOp3.Name = "cbOp3";
            this.cbOp3.Size = new System.Drawing.Size(55, 24);
            this.cbOp3.TabIndex = 7;
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(142, 187);
            this.btnReset.Margin = new System.Windows.Forms.Padding(4);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(84, 26);
            this.btnReset.TabIndex = 10;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnRunSearch
            // 
            this.btnRunSearch.Location = new System.Drawing.Point(39, 187);
            this.btnRunSearch.Margin = new System.Windows.Forms.Padding(4);
            this.btnRunSearch.Name = "btnRunSearch";
            this.btnRunSearch.Size = new System.Drawing.Size(84, 26);
            this.btnRunSearch.TabIndex = 9;
            this.btnRunSearch.Text = "Run Search";
            this.btnRunSearch.UseVisualStyleBackColor = true;
            this.btnRunSearch.Click += new System.EventHandler(this.btnRunSearch_Click);
            // 
            // dtGV
            // 
            this.dtGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGV.Location = new System.Drawing.Point(39, 221);
            this.dtGV.Margin = new System.Windows.Forms.Padding(4);
            this.dtGV.MultiSelect = false;
            this.dtGV.Name = "dtGV";
            this.dtGV.ReadOnly = true;
            this.dtGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtGV.Size = new System.Drawing.Size(1300, 427);
            this.dtGV.TabIndex = 11;
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(39, 682);
            this.btnSelect.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(84, 26);
            this.btnSelect.TabIndex = 12;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(142, 682);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(84, 26);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.xToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1369, 28);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // xToolStripMenuItem
            // 
            this.xToolStripMenuItem.Name = "xToolStripMenuItem";
            this.xToolStripMenuItem.Size = new System.Drawing.Size(30, 24);
            this.xToolStripMenuItem.Text = "X";
            this.xToolStripMenuItem.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(35, 34);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "Filter 1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(35, 84);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "Filter 2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 131);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "Filter 3";
            // 
            // txBxFilter1
            // 
            this.txBxFilter1.Location = new System.Drawing.Point(299, 54);
            this.txBxFilter1.Margin = new System.Windows.Forms.Padding(4);
            this.txBxFilter1.Name = "txBxFilter1";
            this.txBxFilter1.Size = new System.Drawing.Size(167, 22);
            this.txBxFilter1.TabIndex = 2;
            // 
            // txBxFilter3
            // 
            this.txBxFilter3.Location = new System.Drawing.Point(299, 149);
            this.txBxFilter3.Margin = new System.Windows.Forms.Padding(4);
            this.txBxFilter3.Name = "txBxFilter3";
            this.txBxFilter3.Size = new System.Drawing.Size(167, 22);
            this.txBxFilter3.TabIndex = 8;
            this.txBxFilter3.TextChanged += new System.EventHandler(this.txBxFilter3_TextChanged);
            // 
            // txBxFilter2
            // 
            this.txBxFilter2.Location = new System.Drawing.Point(299, 101);
            this.txBxFilter2.Margin = new System.Windows.Forms.Padding(4);
            this.txBxFilter2.Name = "txBxFilter2";
            this.txBxFilter2.Size = new System.Drawing.Size(167, 22);
            this.txBxFilter2.TabIndex = 5;
            // 
            // bindingSource1
            // 
            this.bindingSource1.CurrentChanged += new System.EventHandler(this.bindingSource1_CurrentChanged);
            // 
            // FrmSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumTurquoise;
            this.ClientSize = new System.Drawing.Size(1369, 722);
            this.Controls.Add(this.txBxFilter2);
            this.Controls.Add(this.txBxFilter3);
            this.Controls.Add(this.txBxFilter1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.dtGV);
            this.Controls.Add(this.btnRunSearch);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.cbOp3);
            this.Controls.Add(this.cbOp2);
            this.Controls.Add(this.cbOp1);
            this.Controls.Add(this.cbSchF3);
            this.Controls.Add(this.cbSchF2);
            this.Controls.Add(this.cbSchF1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmSearch";
            this.Text = "Search Window";
            this.Load += new System.EventHandler(this.Search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtGV)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbSchF1;
        private System.Windows.Forms.ComboBox cbSchF2;
        private System.Windows.Forms.ComboBox cbSchF3;
        private System.Windows.Forms.ComboBox cbOp1;
        private System.Windows.Forms.ComboBox cbOp2;
        private System.Windows.Forms.ComboBox cbOp3;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnRunSearch;
        private System.Windows.Forms.DataGridView dtGV;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem xToolStripMenuItem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txBxFilter1;
        private System.Windows.Forms.TextBox txBxFilter3;
        private System.Windows.Forms.TextBox txBxFilter2;
        private System.Windows.Forms.BindingSource bindingSource1;
    }
}