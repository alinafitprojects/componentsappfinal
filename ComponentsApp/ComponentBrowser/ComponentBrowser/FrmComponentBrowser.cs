﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ComponentBrowser
{

    public partial class FrmComponentBrowser : Form
    {
        private DataSet ds;
        private SqlConnection conn;
        private SqlDataAdapter da;
        private Boolean isAdmin;
        private SqlCommandBuilder cmdBldr;
        private BindingSource bs;

        public FrmComponentBrowser(string fName, string lName, bool isAdmin)
        {
            InitializeComponent();
            this.Text = $"Welcome, {fName} {lName}";
            this.isAdmin = isAdmin;
            DisableNonAdminFetures();
            ItemsEditable(false);
        }

        /// <summary>
        /// Hide Admin buttons for Normal Users
        /// </summary>
        private void DisableNonAdminFetures()
        {
            if (isAdmin == false)
            {
                btnAdd.Hide();
                btnDelete.Hide();
                btnEdit.Hide();
                btnCancel.Hide();
                btnUpload.Hide();
                btnSave.Hide();
            }      
        }

        /// <summary>
        /// Enable / Disable fields while components are in Edit / Add Status or just View
        /// </summary>
        /// <param name="edit"></param>
        private void ItemsEditable(bool edit)
        {
            bxImage.AllowDrop = edit;
            foreach (Control control in this.Controls)
            {
                if (control.GetType() == typeof(TextBox))
                {
                    if (control.Name != "txBxSKU")
                    {
                        ((TextBox)control).ReadOnly = !edit;
                    }
                    ((TextBox)control).BackColor = System.Drawing.SystemColors.Window;
                }
                else if (control.GetType() == typeof(ComboBox))
                {
                    ((ComboBox)control).Enabled = edit;
                    ((ComboBox)control).BackColor = System.Drawing.SystemColors.Window;
                }
                else if (control.GetType() == typeof(DateTimePicker))
                {
                    ((DateTimePicker)control).Enabled = edit;
                    ((DateTimePicker)control).BackColor = System.Drawing.SystemColors.Window;
                }
                else if (control.GetType() == typeof(RichTextBox))
                {
                    ((RichTextBox)control).Enabled = edit;
                    ((RichTextBox)control).BackColor = Color.White; ;
                }
                else if (control.GetType() == typeof(CheckBox))
                {
                    ((CheckBox)control).Enabled = edit;
                }
                else if (control.GetType() == typeof(Button) && control.Name == "btnUpload")
                {
                    ((Button)control).Enabled = edit;
                    ((Button)control).BackColor = System.Drawing.SystemColors.Window;
                }
            }
        }

        private void ExitApp(object sender, FormClosedEventArgs e)
        {
            Application.Exit(); //exit application
        }

        /// <summary>
        /// Exit the application with / without save changes confirmation box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            if (isAdmin && txBxName.ReadOnly == false)
            {
                DialogResult result = MessageBox.Show("Do you want to save any changes to the current record before you exit?",
                "Unsaved changes",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Question);

                if (result.ToString() == "Yes")
                {
                    // save data and exit
                    btnSave_Click(btnSave, EventArgs.Empty);
                    if (conn.State != ConnectionState.Closed)
                    {
                        conn.Close();
                    }
                    Application.Exit();
                }
                else if (result.ToString() == "No")
                {
                    // drop changes and exit
                    bs.CancelEdit();
                    ds.RejectChanges();
                    if (conn.State != ConnectionState.Closed)
                    {
                        conn.Close();
                    }
                    Application.Exit();
                }
                else if (result.ToString() == "Cancel")
                {
                    // Do not exit, don't save/drop changes
                }
            } 
            else
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
                Application.Exit(); //exit application
            }
        }

        /// <summary>
        /// Main method to load the windows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmComponentBrowser_Load(object sender, EventArgs e)
        {
            try
            {
                // get db connection
                conn = ConnectToDB.GetDbConnection();

                //initialise the Dataset Object
                ds = new DataSet();

                //create our query string to retrieve all component data
                string selectAllComps = "SELECT * FROM Components";

                //use an SQL data adaptor to retreive all the components table data from the DB and fill the Dataset 'ds' with it
                da = new SqlDataAdapter(selectAllComps, conn);

                SqlCommandBuilder cmdBldr = new SqlCommandBuilder(da);

                //fill the dataset with the retrieved  data
                da.Fill(ds);

                //initlialise the binding source object and set it's dataSource which is the first table in the DataSet (ds.table[0])
                bs = new BindingSource();
                bs.DataSource = ds.Tables[0]; //first table in the collection 

                //update the record display
                UpdateRecordDisplay();

                SetComponentImage();

                // SetImageAndUpdateRecord is called when the bs Postionchanged event occurs
                bs.PositionChanged += SetImageAndUpdateRecord;

                //bind some of our dataset to our form controls by way of using the Binding Source object 'bs'
                //we are binding the "Text" property of the txBxSKU textbox to the datamember  named "Id"  through our BindingSource
                
                //new Binnding (string propertyName, object dataSource, string dataMember);
                txBxSKU.DataBindings.Add(new Binding("Text", bs, "SKU"));

                //Build a list
                var dataSourceCategories = new List<Category>();

                // generate a list of categories and subcategories
                var subCatHardware = new List<string>() { "Motherboard", "CPU", "Graphic Card", "Memory", "HDD", "Notebook", "PC"};
                var subCatSoftware = new List<string>() { "Windows", "Linux", "Office", "Adobe" };
                var subCatPeripherals = new List<string>() { "Mouse", "Keyboard", "Headphone", "Mic", "Monitor" };

                dataSourceCategories.Add(new Category() { Name = "Hardware", SubCategory = subCatHardware });
                dataSourceCategories.Add(new Category() { Name = "Peripherals", SubCategory = subCatPeripherals });
                dataSourceCategories.Add(new Category() { Name = "Software", SubCategory = subCatSoftware });

                //Setup data binding for the caegory combobox
                cbBxCat.DataSource = dataSourceCategories;
                cbBxCat.DisplayMember = "Name";
                cbBxCat.ValueMember = "Name";

                cbBxCat.DataBindings.Add(new Binding("Text", bs, "Category"));
                cbBxSubCat.DataBindings.Add(new Binding("Text", bs, "Subcategory"));
                txBxName.DataBindings.Add(new Binding("Text", bs, "Name"));
                txBxPrice.DataBindings.Add(new Binding("Text", bs, "Price"));

                //format the price 
                txBxPrice.DataBindings[0].FormattingEnabled = true;
                txBxPrice.DataBindings[0].FormatString = "C";

                bxDescription.DataBindings.Add(new Binding("Text", bs, "Description"));

                //the default value of the available checkbox is true
                //we need this in order to add a new record to the bs
                ds.Tables[0].Columns["Available"].DefaultValue = true;
                ckBxAvailable.DataBindings.Add(new Binding("Checked", bs, "Available"));

            }
            catch (Exception ex)
            {
                MessageBox.Show("Database error: " + ex.Message);
            }
        }

        /// <summary>
        /// Load SubCategory combobox values based on Category selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbBxCat_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbBxSubCat.Items.Clear();
            foreach (var item in ((Category)cbBxCat.SelectedItem).SubCategory)
            {
                cbBxSubCat.Items.Add(item);
            }
        }

        private void SetImageAndUpdateRecord(object sender, EventArgs e)
        {
            SetComponentImage();
            UpdateRecordDisplay();
        }

        // Navigation action
        private void btnLast_Click(object sender, EventArgs e)
        {
            ItemsEditable(false);
            bs.MoveLast();           
        }

        // Navigation action
        private void btnNext_Click(object sender, EventArgs e)
        {
            ItemsEditable(false);
            bs.MoveNext();          
        }

        // Navigation action
        private void btnPrevious_Click(object sender, EventArgs e)
        {
            ItemsEditable(false);
            bs.MovePrevious();
           
        }

        // Navigation action
        private void btnFirst_Click(object sender, EventArgs e)
        {
            ItemsEditable(false);
            bs.MoveFirst();
        }

        // Update Display info
        private void UpdateRecordDisplay()
        {
            txBxDisplay.Text = (bs.Position + 1) + " of " + bs.Count;
        }

        // Set/Load PictureBox Image
        private void SetComponentImage()
        {
            try
            {
                //get the image data for the current row for the tabel[0] in dataset
                //cast the returned object t byte array and save it to a byte array
                byte[] imageData = (byte[])(ds.Tables[0].Rows[bs.Position]["Image"]);

                // MessageBox.Show(String.Format("{0}", imageData));

                //Then create an MemoryStream object
                MemoryStream imageStream = new MemoryStream(imageData);

                //Use the MemoryStream object to create an Image and set it as the picture box image
                bxImage.Image = Image.FromStream(imageStream);
            }
            catch (Exception)
            {

                //ignore execption
            }
                
            
            
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            FrmSearch search = new FrmSearch(ds, this);
            search.Show();
            Hide();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                bs.AddNew();
                ItemsEditable(true);
                bxImage.Image = null;

                btnSearch.Enabled = false;
                btnDelete.Enabled = false;
                btnEdit.Enabled = false;
                btnFirst.Enabled = false;
                btnNext.Enabled = false;
                btnPrevious.Enabled = false;
                btnLast.Enabled = false;

                ckBxAvailable.CheckState = CheckState.Unchecked;
            }
            catch (Exception)
            {
                MessageBox.Show("The product cannot be added in the database!" +
                    "Please check the values you inserted in the fields and hit Cancel button.");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //cancel any current edit on the binding source which will revert the data in all fields (textboxes etc.)
            //back to the initial values 
            
            bs.CancelEdit();
            //reject any changes made to the dataset since the last changes where accepted

            ds.RejectChanges();
            //call our method to reset the picture in the GUI to the one (the byte []) in the dataset

            SetComponentImage();

            UpdateRecordDisplay();
            ItemsEditable(false);
            btnSearch.Enabled = true;
            btnDelete.Enabled = true;
            btnEdit.Enabled = true;
            btnAdd.Enabled = true;
            btnFirst.Enabled = true;
            btnNext.Enabled = true;
            btnPrevious.Enabled = true;
            btnLast.Enabled = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                bs.EndEdit();
                SaveItemImageToDataSet();
                da.Fill(ds);
                int recordsUpdate = da.Update(ds);

                UpdateRecordDisplay();
                ItemsEditable(false);
                btnSearch.Enabled = true;
                btnDelete.Enabled = true;
                btnEdit.Enabled = true;
                btnAdd.Enabled = true;
                btnFirst.Enabled = true;
                btnNext.Enabled = true;
                btnPrevious.Enabled = true;
                btnLast.Enabled = true;
                MessageBox.Show(recordsUpdate + " records were updated in the database.");
            }         
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                MessageBox.Show(" The article cannot ba added in the database!"
                    + "\nPlease check the values entered in all fields or hit the Cancel button.  ");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you really want to delete this record?",
                "Confirm Delete",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question);

            if (result.ToString() == "OK")
            {
                if (bs.Count > 0)
                {
                    //remove the current item from the list (bs)
                    bs.RemoveCurrent();

                    //commit the changes to the dataset
                    ds.AcceptChanges();
                    da.Update(ds);
                    //fix the recor display to show the correct number of records
                    UpdateRecordDisplay();
                    //call the setcomponentImage method to retrieve the current rows image data  from the dataseta and use it to set the imahe for the current item

                    SetComponentImage();
                }
            }
            else if (result.ToString() == "Cancel")
            {
                // do nothing
            }
            
        }

        // change binding source position based on SKU
        public void MoveToComponentSKU(int sku)
        {
            int itemIndex = bs.Find("sku", sku);
            bs.Position = itemIndex;
        }

        private void bxImage_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void bxImage_DragDrop(object sender, DragEventArgs e)
        {
            if (isAdmin)
            {
                //an array of string wich will hold the image file path and names
                string[] imageNames = (string[])e.Data.GetData(DataFormats.FileDrop);

                //create a new image from the first file name in the array
                Image newPicture = Image.FromFile(imageNames[0]);

                //set the new image for the pictureBox object
                bxImage.Image = newPicture;

                //SaveItemImageToDataSet();
            }
            
        }

        /// <summary>
        /// save the image to the data set
        /// </summary>
        private void SaveItemImageToDataSet()
        {
            //create a new Memory Stream object
            MemoryStream imageData = new MemoryStream();

            //save the image  to the Momory Stream object
            bxImage.Image.Save(imageData, System.Drawing.Imaging.ImageFormat.Bmp);

            ds.Tables[0].Rows[bs.Position]["Image"] = imageData.ToArray();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            bxImage.AllowDrop = true;
            ItemsEditable(true); 
            btnUpload.Show();

            btnSearch.Enabled = false;
            btnDelete.Enabled = false;
            btnAdd.Enabled = false;
            btnFirst.Enabled = false;
            btnNext.Enabled = false;
            btnPrevious.Enabled = false;
            btnLast.Enabled = false;
        }

       
        private void btnUpload_Click(object sender, EventArgs e)
        {
            //create a new instance of OpenFile Dialog
            OpenFileDialog filePicker = new OpenFileDialog();

            //set the filter for the file tpes that the dialogues shows
            filePicker.InitialDirectory = @"c:\components_img";

            filePicker.Filter = "JPEG Files (*.jpg)|*.jpg|PNG Files (*.png)|*.png|BMP Files(*.bmp)| *.bmp| All files (*.*)|*.*";

            //if the showDialog results in the ok butoon being pressed then set the picture box image 
            if (filePicker.ShowDialog() == DialogResult.OK)
            {
                bxImage.Load(filePicker.FileName);
                //ds.Tables[0].Rows[bs.Position]["Image"] = bxImage.ToString();
            }
        }

        private void bxImage_Click(object sender, EventArgs e)
        {

        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (isAdmin == false)
            {
                MessageBox.Show("For navigation use the buttons First, Previous, Next and Last.\nFor searching a specific product click on button Search.\nFor closing the application click on button Close.");
            }
            else
            {
                MessageBox.Show("For navigation use the buttons First,Previous, Next and Last.\nFor searching a specific product click on button Search.\nFor adding a new product click buttons Edit Current, Add New, Save.\nFor deleting a product click button Delete.\nFor updating a product that already exist in the database click on buttons Edit Current, after that go to the field that need updates from Name, Price, Category, Subcategory, Date First In Stock and Available box.\nFor canceling any action/change produced to the product click on button Cancel.\nFor closing the application click on button Close.");
            }
                
        }
    }

    /// <summary>
    /// Class for storing the Name of a Main Category and a list of SubCategories
    /// </summary>
    public class Category
    {
        public string Name { get; set; }
        public List<string> SubCategory = new List<string>();
        public Category()
        {
        }
    }
}
