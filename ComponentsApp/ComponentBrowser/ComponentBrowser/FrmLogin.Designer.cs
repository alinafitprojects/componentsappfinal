﻿namespace ComponentBrowser
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txBxUsername = new System.Windows.Forms.TextBox();
            this.txBxPassword = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.pgBarLogin = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(141, 56);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(141, 103);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txBxUsername
            // 
            this.txBxUsername.Location = new System.Drawing.Point(231, 56);
            this.txBxUsername.Margin = new System.Windows.Forms.Padding(4);
            this.txBxUsername.Name = "txBxUsername";
            this.txBxUsername.Size = new System.Drawing.Size(132, 22);
            this.txBxUsername.TabIndex = 2;
            // 
            // txBxPassword
            // 
            this.txBxPassword.Location = new System.Drawing.Point(231, 103);
            this.txBxPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txBxPassword.Name = "txBxPassword";
            this.txBxPassword.PasswordChar = '*';
            this.txBxPassword.Size = new System.Drawing.Size(132, 22);
            this.txBxPassword.TabIndex = 3;
            this.txBxPassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TryLoginOnEnterKey);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(231, 143);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(132, 36);
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // pgBarLogin
            // 
            this.pgBarLogin.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pgBarLogin.Location = new System.Drawing.Point(144, 195);
            this.pgBarLogin.Name = "pgBarLogin";
            this.pgBarLogin.Size = new System.Drawing.Size(219, 20);
            this.pgBarLogin.Step = 30;
            this.pgBarLogin.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgBarLogin.TabIndex = 5;
            this.pgBarLogin.Click += new System.EventHandler(this.pgBarLogin_Click);
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumTurquoise;
            this.ClientSize = new System.Drawing.Size(492, 321);
            this.Controls.Add(this.pgBarLogin);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.txBxPassword);
            this.Controls.Add(this.txBxUsername);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmLogin";
            this.Text = "Component Browser Login";
            this.Load += new System.EventHandler(this.FrmLogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txBxUsername;
        private System.Windows.Forms.TextBox txBxPassword;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.ProgressBar pgBarLogin;
    }
}

