﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace ComponentBrowser
{
    public partial class FrmLogin : Form
    {
        //SQL database connection
        private SqlConnection dbConnection;

        public FrmLogin()
        {
            InitializeComponent();
            dbConnection = ConnectToDB.GetDbConnection();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            pgBarLogin.Step = 33;

            try
            {   //try and get databasae connection
                if (dbConnection == null || dbConnection.State == ConnectionState.Closed)
                {
                    dbConnection.Open();
                }
                pgBarLogin.PerformStep();
                if (!String.IsNullOrEmpty(txBxUsername.Text)
                    && !String.IsNullOrEmpty(txBxPassword.Text))
                {
                    var sha1 = new SHA1CryptoServiceProvider();
                    byte[] bytes = Encoding.UTF8.GetBytes(txBxPassword.Text);
                    string HashedPasswords = Convert.ToBase64String(sha1.ComputeHash(bytes));

                    // vheck if user / password combo is present in the database
                    string findUser = $"SELECT * FROM Users WHERE Username = @username AND Password = @password";

                    // Make an SQLCommand
                    SqlCommand searchForUser = new SqlCommand(findUser, dbConnection);

                    //Make a Sql Parameters
                    SqlParameter uName = new SqlParameter("@username", SqlDbType.NVarChar);
                    SqlParameter pw= new SqlParameter("@password", SqlDbType.Char);

                    //set the values out of parameters
                    uName.Value = txBxUsername.Text.Trim().ToLower();
                    pw.Value = HashedPasswords;

                    //Add the paremeters to the Sqlcommand
                    searchForUser.Parameters.Add(uName);
                    searchForUser.Parameters.Add(pw);

                    // Execute the command and read the results
                    SqlDataReader results = searchForUser.ExecuteReader();

                    pgBarLogin.PerformStep();

                    // If records
                    if (results.HasRows)
                    {
                        // and read the first result/record
                        results.Read();
                        pgBarLogin.PerformStep();

                        MessageBox.Show("Welcome " + results.GetString(1) +"\n\nYour user ID is: " + results.GetInt32(0));

                        // send first name, last name and if is admin to new window
                        FrmComponentBrowser componentBrowwserWindow = new FrmComponentBrowser(results.GetString(1), results.GetString(2), results.GetBoolean(5));

                        // Close the results set
                        results.Close();
                        componentBrowwserWindow.Show();
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("User not found!");
                        txBxUsername.Clear();
                        txBxPassword.Clear();
                        results.Close();
                        pgBarLogin.Value = 0;
                    }
                }
                else
                {
                    MessageBox.Show("Please check you have enetered both a username and password!");
                    pgBarLogin.Value = 0;
                }

            }
            catch (SqlException ex)
            {
                MessageBox.Show("Database connection problem: \n" + ex.Message);
            }


        }

        private void TryLoginOnEnterKey(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)

            {
                btnLogin_Click(null, null);
            }
        }

        private void CreateUsername(SqlConnection dbConnection)
        {
            var sha1 = new SHA1CryptoServiceProvider();
            byte[] bytes = Encoding.UTF8.GetBytes("333333");
            string HashedPasswords = Convert.ToBase64String(sha1.ComputeHash(bytes));

            MessageBox.Show(HashedPasswords);

            string insertUser = "INSERT INTO Users Values ('Kate', 'White', 'Kwhite', '" + HashedPasswords + "', 0)";

            // Make an SQLCommand
            SqlCommand insertCmd = new SqlCommand(insertUser, dbConnection);
            insertCmd.ExecuteNonQuery();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pgBarLogin_Click(object sender, EventArgs e)
        {

        }
    }
}
